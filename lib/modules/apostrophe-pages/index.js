module.exports = {
  types: [
    {
      name: 'home',
      label: 'Home'
    },
    {
      name: 'texts-pages',
      label: 'Texts'
    },
    {
      name: 'galleries-pages',
      label: 'Gallery'
    },
    {
      name: 'sections-pages',
      label: 'Sections',
    },
    {
      name: 'people-pages',
      label: 'People'
    },
    {
      name: 'tables-pages',
      label: 'Tables'
    }
  ]
};
